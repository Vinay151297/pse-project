//-----------------------------------------------------------------------
// <author> 
//     Parth Patel
// </author>
//
// <date> 
//     26-09-2018 
// </date>
// 
// <reviewer> 
//     Libin N George 
// </reviewer>
// 
// <copyright file="ICommunication.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      Interface and other essential types and delegates for Communication Class
// </summary>
//-----------------------------------------------------------------------
namespace Masti.Networking
{
    using System.Net;

    /// <summary>
    /// Prototype of function triggered when data is received
    /// </summary>
    /// <param name="data"> data recived (string)</param>
    /// <param name="fromIP">Ipaddress from which data is recived</param>
    public delegate void DataReceivalHandler(string data, IPAddress fromIP);

    /// <summary>
    /// Prototype of function triggered to notify the status of data transfer
    /// </summary>
    /// <param name="data">data being sent</param>
    /// <param name="status">status(success or failure)</param>
    public delegate void DataStatusHandler(string data, StatusCode status);

    /// <summary>
    /// Status after sending
    /// </summary>
    public enum StatusCode
    {
        /// <summary>
        /// Send Success 
        /// </summary>
        Success,

        /// <summary>
        /// Send Failure
        /// </summary>
        Failure
    }

    /// <summary>
    /// Module type
    /// </summary>
    public enum DataType
    {
        /// <summary>
        /// message module
        /// </summary>
        Message,

        /// <summary>
        /// Image sharing module
        /// </summary>
        ImageSharing
    }

    /// <summary>
    /// Icommunication Interface
    /// </summary>
    public interface ICommunication
    {
        /// <summary>
        /// Sends the data(message) to Target IP
        /// </summary>
        /// <param name="msg">Message or Data to be send</param>
        /// <param name="dataID"> Message ID </param>
        /// <param name="targetIP">IP to which data has to be send </param>
        /// <param name="type">Data type of the message (Message or ImageSharing)</param>
        /// <returns>returns True if successfully added to the queue otherwise False</returns>
        bool Send(string msg, ulong dataID, IPAddress targetIP, DataType type);

        /// <summary>
        /// Subcribes to get notified by event handler in receving packets specified by given type
        /// </summary>
        /// <param name="type">Data type for which subscription has to be done </param>
        /// <param name="receivalHandler">Event handler to be called on reciving Data</param>
        /// <returns>returns true on sucessfully subscribing</returns>
        bool SubscribeForDataReceival(DataType type, DataReceivalHandler receivalHandler);

        /// <summary>
        /// Subcribes to get notified by event handler in sending packets specified by given type
        /// </summary>
        /// <param name="type">Data type of message</param>
        /// <param name="statusHandler">Event handler to be called on sucessfully sending data</param>
        /// <returns>returns true on sucessfully subscribing</returns>
        bool SubscribeForDataStatus(DataType type, DataStatusHandler statusHandler);
    }

    /// <summary>
    /// Interface for SingletoneFactory that produces ICommunication objects
    /// </summary>
    public interface ICommunicationFactory
    {
        /// <summary>
        /// Returns ICommunicator Instance 
        /// </summary>
        /// <returns>Returns instance of class that implements this inerface.</returns>
        ICommunication GetCommunicator();
    }
}
